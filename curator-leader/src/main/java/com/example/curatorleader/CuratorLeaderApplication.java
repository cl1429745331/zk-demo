package com.example.curatorleader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuratorLeaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(CuratorLeaderApplication.class, args);
    }

}
