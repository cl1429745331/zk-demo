/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50715
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 50715
 File Encoding         : 65001

 Date: 27/02/2022 20:01:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods_stock
-- ----------------------------
DROP TABLE IF EXISTS `goods_stock`;
CREATE TABLE `goods_stock`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `goods_no` int(11) NOT NULL COMMENT '商品编号',
  `stock` int(11) NULL DEFAULT NULL COMMENT '库存',
  `isActive` smallint(6) NULL DEFAULT NULL COMMENT '是否上架（1上，0不是）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_stock
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
