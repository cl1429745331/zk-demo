package com.example.zookeeperlockdemo.service;

import com.example.zookeeperlockdemo.dal.model.GoodsStock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chen
 * @since 2022-02-27
 */
public interface IGoodsStockService extends IService<GoodsStock> {

}
