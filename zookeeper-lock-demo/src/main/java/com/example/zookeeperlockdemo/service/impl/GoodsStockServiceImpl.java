package com.example.zookeeperlockdemo.service.impl;

import com.example.zookeeperlockdemo.dal.model.GoodsStock;
import com.example.zookeeperlockdemo.dal.mapper.GoodsStockMapper;
import com.example.zookeeperlockdemo.service.IGoodsStockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chen
 * @since 2022-02-27
 */
@Service
public class GoodsStockServiceImpl extends ServiceImpl<GoodsStockMapper, GoodsStock> implements IGoodsStockService {

}
