package com.example.zookeeperlockdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.zookeeperlockdemo.dal.mapper")
public class ZookeeperLockDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZookeeperLockDemoApplication.class, args);
    }

}
