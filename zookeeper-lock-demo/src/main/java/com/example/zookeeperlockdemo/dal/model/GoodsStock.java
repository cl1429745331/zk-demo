package com.example.zookeeperlockdemo.dal.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author chen
 * @since 2022-02-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsStock extends Model {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品编号
     */
    private Integer goodsNo;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 是否上架（1上，0不是）
     */
    @TableField("isActive")
    private Integer isactive;


}
