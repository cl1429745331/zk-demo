package com.example.zookeeperlockdemo.dal.mapper;

import com.example.zookeeperlockdemo.dal.model.GoodsStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2022-02-27
 */
public interface GoodsStockMapper extends BaseMapper<GoodsStock> {

}
